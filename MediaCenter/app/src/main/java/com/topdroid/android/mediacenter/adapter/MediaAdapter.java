package com.topdroid.android.mediacenter.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.topdroid.android.mediacenter.R;
import com.topdroid.android.mediacenter.vo.MediaCard;

import java.util.List;

/**
 * Created by rakesh.barik on 16-07-2016.
 */
public class MediaAdapter extends  RecyclerView.Adapter<CardItemViewHolder> {
    private List<MediaCard> itemList;
    private Context context;
    public static final int VIEW_TYPE_SECTION_HEADER = 0;
    public static final int VIEW_TYPE_IMAGES = 1;
    private static final int VIEW_TYPE_VIDEOS = 2;
    private static final int VIEW_TYPE_DOCS = 3;
    private static final int VIEW_TYPE_PDF = 4;
    private static final int VIEW_TYPE_AUDIO = 5;

    public MediaAdapter(Context context, List<MediaCard> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public CardItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView;
        CardItemViewHolder cardItemViewHolder = null;
        switch (viewType){
            case VIEW_TYPE_SECTION_HEADER:
                layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.section_header, null);
                cardItemViewHolder = new CardItemViewHolder(layoutView,VIEW_TYPE_SECTION_HEADER);
                break;
            case VIEW_TYPE_IMAGES:
                layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.media_item_view, null);
                cardItemViewHolder = new CardItemViewHolder(layoutView,VIEW_TYPE_IMAGES);
        }
        return cardItemViewHolder;
    }

    @Override
    public void onBindViewHolder(CardItemViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType){
            case VIEW_TYPE_SECTION_HEADER:
                StaggeredGridLayoutManager.LayoutParams layoutParams = new StaggeredGridLayoutManager.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setFullSpan(true);
                holder.sectionHeaderView.setLayoutParams(layoutParams);
                holder.sectionHeaderView.setText("Images");
                break;
            case VIEW_TYPE_IMAGES:
                Glide.with(context).load(itemList.get(position).getUrl()).into(holder.mediaView);
        }

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        switch (position){
            case 0:
                viewType = VIEW_TYPE_SECTION_HEADER;
            break;
            default:
                viewType = VIEW_TYPE_IMAGES;
                break;
        }
        return viewType;
    }
}

 class CardItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView mediaView;
    public TextView sectionHeaderView;

    public CardItemViewHolder(View itemView,int viewType) {
        super(itemView);
        if(viewType == MediaAdapter.VIEW_TYPE_SECTION_HEADER){
            sectionHeaderView = (TextView) itemView.findViewById(R.id.tv_section_header);
        }else{
            itemView.setOnClickListener(this);
            mediaView = (ImageView) itemView.findViewById(R.id.image_item);
        }
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}

