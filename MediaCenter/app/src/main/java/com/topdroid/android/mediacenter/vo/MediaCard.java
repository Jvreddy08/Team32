package com.topdroid.android.mediacenter.vo;

/**
 * Created by rakesh.barik on 16-07-2016.
 */
public class MediaCard {
    private String url;

    public MediaCard(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
