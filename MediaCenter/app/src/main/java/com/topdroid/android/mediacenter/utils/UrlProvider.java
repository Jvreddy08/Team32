package com.topdroid.android.mediacenter.utils;

import com.topdroid.android.mediacenter.vo.MediaCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rakesh.barik on 16-07-2016.
 */
public class UrlProvider {
    public static List<MediaCard> getImageUrlList() {
        List<MediaCard> imageList = new ArrayList<>();
        imageList.add(new MediaCard("http://farm8.staticflickr.com/7111/7429651528_a23ebb0b8c.jpg"));
        imageList.add(new MediaCard("http://farm9.staticflickr.com/8288/7525381378_aa2917fa0e.jpg"));
        imageList.add(new MediaCard("http://farm8.staticflickr.com/7127/7675112872_e92b1dbe35.jpg"));
        imageList.add(new MediaCard("http://farm9.staticflickr.com/8182/7941954368_3c88ba4a28.jpg"));
        imageList.add(new MediaCard("http://farm9.staticflickr.com/8306/7987149886_6535bf7055.jpg"));
        imageList.add(new MediaCard("http://thejoburg.com/wp-content/uploads/2014/12/fashion-model-89126.jpg"));
        imageList.add(new MediaCard("http://farm9.staticflickr.com/8483/8218023445_02037c8fda.jpg"));
        return imageList;
    }
}
